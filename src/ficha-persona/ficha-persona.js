import { LitElement, html} from 'lit-element';

class FichaPersona  extends LitElement {

    static get properties () {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            photo: {type: Object}
        };
    }

    constructor(){
        super();

        this.name="Prueba nombre";
        this.yearsInCompany=12;
        this.photo = {
            src: "./img/persona.jpg",
            alt: "foto persona"
        };

        this.updatePersonInfo();
    }

    //Función que detecte los cambios
    updated(changedProperties){
        changedProperties.forEach((oldValue,propName)=> {
            console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue);
        }) ;

        if (changedProperties.has("name")){
            console.log("Propiedad name cambia valora anterior era " + 
            changedProperties.get("name") + " nuevo es " + this.name)
        }

        if (changedProperties.has("yearsInCompany")){
            console.log("Propiedad name cambia valora anterior era " + 
            changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }
//La plantilla lo único que debe hacer es presentar el contenido del componente
    render(){ 
        return html`
            <div> 
                <label> Nombre Completo </label>
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label> Años en la empresa </label>
                <input type="text" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" value="${this.personInfo}" disabled ></input>
                <br />
                <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}">
            </div>
        `;
    }
    //Función actualizar nombre
    updateName(e){
            console.log("updateName");
            console.log("Actualizando propiedad nombre con el valor " + e.target.value );
            this.name=e.target.value;
    }

    //Función actualizar años
    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        console.log("Actualizando propiedad yearsInCompany con el valor " + e.target.value );
        this.yearsInCompany=e.target.value;
    }

    //Función actualiza la categoría en función de los años que lleva en la empresa
    updatePersonInfo(e) {
        if (this.yearsInCompany >= 7){
               this.personInfo= "lead";
           } else if (this.yearsInCompany >= 5){
               this.personInfo= "senior";
           } else if (this.yearsInCompany >= 3){
               this.personInfo= "team";
           } else
           this.personInfo= "junior";
        }
      
}

customElements.define('ficha-persona', FichaPersona)