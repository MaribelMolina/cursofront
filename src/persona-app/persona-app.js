import { LitElement, html} from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';



class PersonaApp  extends LitElement {

    //Creamos propiedad desde donde voy a recoger la información
    static get properties(){
        return {
            peopleAct: {type: Array},
            peopleFilt:{type:Number}
        };
    }

    constructor (){
        super();
    }

    render(){ 
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">     
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar class="col-2" 
                            @new-person="${this.newPerson}"
                            @updated-people-filt="${this.updatePeopleFilt}"
                >
                </persona-sidebar>
                <persona-main id="PersonaActual" class="col-10" @updated-people="${this.updatedPeople}" >
                </persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats .people="${this.peopleAct}"  
                         @updated-people-stats="${this.updatedPeopleStats}"
            >
            </persona-stats>

        `;
    }

    updatePeopleFilt(e){
        console.log("Se obtiene el filtro range en app")
        console.log(e.detail.peopleFilt)
        this.peopleFilt=e.detail.peopleFilt;
        this.shadowRoot.querySelector('persona-main').peopleFilt = this.peopleFilt
    
    }
    
    updatedPeople(PersonaActual){
        console.log("Se ha obtenido el array de personas actualizadas");
        console.log(PersonaActual.detail.people);
        this.peopleAct=PersonaActual.detail.people;
    }

    updatedPeopleStats(e){
        console.log("Capturamos el evento contador de stats")
        console.log(e.detail);
        this.shadowRoot.querySelector('persona-sidebar').peopleStats = e.detail.peopleStats;
        this.shadowRoot.querySelector('persona-main').peopleFilt = e.detail.peopleStats.maxYearsInCompany;

     }

    newPerson(e){
        console.log("newPerson en persona-app");
        this.shadowRoot.querySelector("persona-main").showPersonForm=true;
    }
}

customElements.define('persona-app', PersonaApp)