import { LitElement, html} from 'lit-element';

class PersonaMainDM  extends LitElement {
    static get properties(){
        return {
            people: {type: Array}
        }
    }

    constructor(){
        super();
        this.people =[
            {
                name: "Maribel Molina",
                yearsInCompany:10,
                profile: "lorem blablblab",
                photo: {
                    src: "./img/persona1.jfif",
                    alt: "Maribel Molina"

                }
            },
            {
                name: "Antonio del Caño",
                yearsInCompany:2,
                profile: "lorem blablblab",
                photo: {
                    src: "./img/persona2.jfif",
                    alt: "Antonio del Caño"
                }

            },{
                name: "Lucía del Caño",
                yearsInCompany:5,
                profile: "lorem blablblab",
                photo: {
                    src: "./img/persona3.jfif",
                    alt: "Lucía del Caño"
                }
            },
            {
                name: "Pepito Perez",
                yearsInCompany:9,
                profile: "lorem blablblab xerqtqhy",
                photo: {
                    src: "./img/persona4.jfif",
                    alt: "Pepito Perez"
                }
            },
            {
                name: "Maximiliano",
                yearsInCompany:1,
                profile: "lorem blablblab affaferarafar ",
                photo: {
                    src: "./img/persona5.jfif",
                    alt: "Maximiliano"
                }
            }

        ];
    }

    updated(changedProperties){
        console.log("updated");

        if (changedProperties.has("people")){
            console.log ("Ha cambiado el valor de la propiedad people");

            this.dispatchEvent(
                new CustomEvent("people-data-updated",
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            )
        }   
    }
}

customElements.define('persona-main-dm', PersonaMainDM)