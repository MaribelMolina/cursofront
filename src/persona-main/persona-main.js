//import { LitElement, html} from 'lit-element';
import { LitElement, html, css} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js';

class PersonaMain  extends LitElement {

    static get properties(){
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},
            peopleFilt: { type: Number },
        };
    }

    //Para excepcionar el estilo
    /*
    static get styles(){
        return css`
            :host{
                all: initial;
            }
            `        
    }*/

    constructor(){
        super();
        this.peopleFilt = 100;
        this.people =[];
        /*this.people =[
            {
                name: "Maribel Molina",
                yearsInCompany:10,
                profile: "lorem blablblab",
                photo: {
                    src: "./img/persona1.jfif",
                    alt: "Maribel Molina"

                }
            },
            {
                name: "Antonio del Caño",
                yearsInCompany:2,
                profile: "lorem blablblab",
                photo: {
                    src: "./img/persona2.jfif",
                    alt: "Antonio del Caño"
                }

            },{
                name: "Lucía del Caño",
                yearsInCompany:5,
                profile: "lorem blablblab",
                photo: {
                    src: "./img/persona3.jfif",
                    alt: "Lucía del Caño"
                }
            },
            {
                name: "Pepito Perez",
                yearsInCompany:9,
                profile: "lorem blablblab xerqtqhy",
                photo: {
                    src: "./img/persona4.jfif",
                    alt: "Pepito Perez"
                }
            },
            {
                name: "Maximiliano",
                yearsInCompany:1,
                profile: "lorem blablblab affaferarafar ",
                photo: {
                    src: "./img/persona5.jfif",
                    alt: "Maximiliano"
                }
            }

        ];*/

        this.showPersonForm=false;
        
    }

//por cada persona que haya queremos que aparezca una plantilla. Cada persona lo convierte en una persona-ficha-listado
//si ponemos display none (d-none) lo ocultamos 
render(){ 
        return html`

        ------ ${this.peopleFilt} ------
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">      
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        
                            persona =>  html`<persona-ficha-listado
                            fname="${persona.name}" 
                            yearsInCompany="${persona.yearsInCompany}"
                            profile="${persona.profile}"
                            class="${(this.peopleFilt < persona.yearsInCompany)? 'd-none' : '' }"
                            .photo="${persona.photo}"
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}">
                        </persona-ficha-listado>`
                    )}
                </div>
            </div>
            <div class="row">
                    <persona-form id="personaForm" class="d-none" @persona-form-close="${this.personFormClose}"
                                                                  @persona-form-store="${this.personFormStore}"></persona-form>
            </div>
            <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>
        `;
    }

    peopleDataUpdated(e){
        this.people=e.detail.people;
    }
    updated(changedProperties){
        console.log("updated");

        if(changedProperties.has("showPersonForm")){
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona main")
        
            if(this.showPersonForm === true){
                this.showPersonFormData();
            }else{
                this.showPersonList();
            }
        }

        //Para que litElement considere que un array se modifica ... se tiene que modificar el array entero... no un elemento del array 
        if (changedProperties.has("people")){

            console.log("Ha cambiado el valor de la propiedad people en persona-main")
            this.dispatchEvent(new CustomEvent("updated-people",{
                detail: {
                    people: this.people
                }
            }
            
            ));
        }
    }

    personFormStore(e){
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");
        console.log(e.detail)

      
        console.log("Persona almacenada");

        if(e.detail.editingPerson ==true){
            console.log("Se va a actualizar la persona de nombre" + e.detail.person.name);

            //Actualizo el array people
            this.people = this.people.map(
                person => person.name === e.detail.person.name
                ? person = e.detail.person : person
            )

            let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );
            if (indexOfPerson >=0){
                console.log("Persona encontrada");
                this.people[indexOfPerson]=e.detail.person;
            }
        }
        else {
            console.log("Se va a dar de alta una persona nueva");
            //Los ... es una enumeración de los elementos del array (spread syntax)
            this.people = [...this.people,e.detail.person];
        }
        console.log("Persona almacenada");

        this.showPersonForm=false;

    }

    personFormClose(){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");

        this.showPersonForm =false;
    }

    showPersonFormData(){
        console.log("showPersonFormData");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personaForm").classList.remove("d-none");
    }

    //Mostrar
    showPersonList(){
        console.log("showPersonList");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personaForm").classList.add("d-none");
    }

    //Borrado
    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre "+ e.detail.name);

        //Estoy cambiando el array (filter devuelve un array nuevo)
        this.people = this.people.filter(
            person => person.name != e.detail.name
        )
        console.log(this.people)
        
    }

    //Edición
    infoPerson(e){
        console.log("infoPerson en persona-main");
        console.log("Se ha solicitado más información de " + e.detail.name);

        //filter siempre devuelve un array. Cuidado con esto... porque cuando tengamos que pasarlo tenemos que enviar el elemento del array
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name 
        );
        console.log(chosenPerson);
        //Mandamos el primer elemento del array a personForm para que el formulario muestre las personas
        this.shadowRoot.getElementById("personaForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("personaForm").editingPerson=true;
        this.showPersonForm=true;
    }
}

customElements.define('persona-main', PersonaMain)