import { LitElement, html} from 'lit-element';

class PersonaStats extends LitElement {

    static get properties(){
        return {
            people: {type: Array}
        };
    }

    constructor(){
       
        super();

        //Inicializamos siempre en el constructor
        this.people=[];
        console.log("stats");
        
    }

    updated(changedProperties){
        
        if (changedProperties.has("people")){
            console.log("Entramos a persona stats cuando actualiza el array people")
            
            let peopleStats= this.PersonaContador(this.people);

            this.dispatchEvent(new CustomEvent("updated-people-stats",{
                detail:  {
                    peopleStats : peopleStats
                }
                
            }
            ));

        }
    }

    PersonaContador(e)
    {
        console.log("contador");

        let peopleStats = {};
        peopleStats.contador = e.length;

        let maxYearsInCompany=0;

        e.forEach(person => {
            if (person.yearsInCompany > maxYearsInCompany){
                maxYearsInCompany = person.yearsInCompany;
            }
        })

        peopleStats.maxYearsInCompany=maxYearsInCompany;

        console.log("El contador de personas es " + peopleStats.contador);
        console.log("maxYearsIncompy es " + maxYearsInCompany);
       

        return peopleStats;
    }
    
  
}

customElements.define('persona-stats', PersonaStats)