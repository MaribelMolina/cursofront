import { LitElement, html} from 'lit-element';

class TestApi  extends LitElement {

    static get properties(){
        return {
            movies: {type: Array}
        };

    }

    constructor(){
        super();
        this.movies=[];
        this.getMovieData();
    }
    render(){ 
        return html`
            
            ${this.movies.map(
                movie => html ` <div>La película ${movie.title}, fue dirigida por ${movie.director}</div>`
            )}
        `;
    }

    getMovieData(){
        console.log("getMovieData");
        console.log("Obteniendo datos de las películas")
        //Gestiona peticiones asícnronas y lo asignamos a un xhr
        let xhr= new XMLHttpRequest();
        
        //Se va a llamar cuando se termine de cargar el envío de la petición asíncrona. Captura del evento
        xhr.onload = () => {
            //Triple igual comprueba valor y tipo
            if (xhr.status === 200){
                console.log("Petición completada correctamente");

                //Si hacemos console log de la respuesta vemos que es un json
                console.log(xhr.responseText);
                //al ser un json lo podemos parsear
                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);

                this.movies=APIResponse.results;
            }
        }

        //método y url a la que tiene que llamar (se saca de la API). Abre el canal para preparar el envío
        xhr.open("GET","https://swapi.dev/api/films/");
        //En el send se envía el body en el caso de que tenga body. El GET no tiene body pero el POST sí
        xhr.send();
        console.log("Fin de getMovieData");
    }
}

customElements.define('test-api', TestApi)