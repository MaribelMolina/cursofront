import { LitElement, html,css} from 'lit-element';

class TestBootstrap  extends LitElement {

    static get styles(){

        return css`
        .redbg{
            background-color: red;
        }
        .greenbg{
            background-color: green;
        }
        .bluebg{
            background-color: blue;
        }
        .greybg{
            background-color: grey;
        }
        `;
    }
    render(){ 
        return html`
        <!-- Incluimos un css de bootstrap-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">      
        <h3>Test Bootstrap</h3>
        <div class="row greybg">
        <!-- el ancho que ocupa suele ser fijo. Puedo "flexibilizar" el tamaño que ocupa cada columna-->
            <div class="col-2 col-sm-1 redbg">Col 1</div>
            <div class="col-3 offset-4 greenbg">Col 2</div>
            <div class="col-4 offset-1 bluebg">Col 3</div>

        </div>
        `;
    }
}

customElements.define('test-bootstrap', TestBootstrap)